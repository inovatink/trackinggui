#include "faceDetect.h"

int faceDetect::faceposX = 0;
int faceDetect::faceposY = 0;

faceDetect::faceDetect()
{
}

Mat IplToMatConverter(IplImage* imageToMat)
{
    Mat mat = cvarrToMat(imageToMat);
    return mat;
}

static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, path, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(imread(path, 0));
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}

void faceDetect::initialize()
{
    string fn_haar = "/home/inovatink/src/OpenCV-2.4.2/data/lbpcascades/lbpcascade_frontalface.xml";
    string fn_csv = "/home/inovatink/workspace_qt/trackGUI/csv.ext";
    // These vectors hold the images and corresponding labels.
    vector<Mat> *images = new vector<Mat>;
    vector<int> *labels = new vector<int>;
    // Read in the data. This can fail if no valid
    // input filename is given.
    try {
        read_csv(fn_csv, *images, *labels);
    } catch (cv::Exception& e) {
        cerr << "Error opening file \"" << fn_csv << "\". Reason: " << e.msg << endl;
        // nothing more we can do
        exit(1);
    }
    // Quit if there are not enough images for this demo.
    if(images->size() <= 1) {
        string error_message = "This demo needs at least 2 images to work. Please add more images to your data set!";
        CV_Error(CV_StsError, error_message);
    }

    haar_cascade.load(fn_haar);

    // The following lines simply get the last images from
    // your dataset and remove it from the vector. This is
    // done, so that the training data (which we learn the
    // cv::FaceRecognizer on) and the test data we test
    // the model with, do not overlap.
    images->pop_back();
    labels->pop_back();
    // The following lines create an LBPH model for
    // face recognition and train it with the images and
    // labels read from the given CSV file.
    //
    // The LBPHFaceRecognizer uses Extended Local Binary Patterns
    // (it's probably configurable with other operators at a later
    // point), and has the following default values
    //
    //      radius = 1
    //      neighbors = 8
    //      grid_x = 8
    //      grid_y = 8
    //
    // So if you want a LBPH FaceRecognizer using a radius of
    // 2 and 16 neighbors, call the factory method with:
    //
    //      cv::createLBPHFaceRecognizer(2, 16);
    //
    // And if you want a threshold (e.g. 123.0) call it with its default values:
    //
    //      cv::createLBPHFaceRecognizer(1,8,8,8,123.0)
    //
    model = createLBPHFaceRecognizer();
    model->train(*images, *labels);
}

void faceDetect::lbph_facedetect(IplImage* iplimg)
{
    Mat original = IplToMatConverter(iplimg);
    // Convert the current frame to grayscale:
    Mat gray;
    cvtColor(original, gray, CV_BGR2GRAY);

    // Find the faces in the frame:
    vector< Rect_<int> > faces;
    haar_cascade.detectMultiScale(gray, faces);
    // At this point you have the position of the faces in
    // faces. Now we'll get the faces, make a prediction and
    // annotate it in the video. Cool or what?
    for(int i = 0; i < faces.size(); i++) {
        // Process face by face:
        Rect face_i = faces[i];
        Mat face = gray(face_i);
        model->set("threshold", 0.0);
        model->predict(face);
        // And finally write all we've found out to the original image!
        // First of all draw a green rectangle around the detected face:
        string box_text = "";

        //check if there hat is present
        if(abs(faceDetect::faceposX - shared::hatposX)<35)
        {
            shared::hatFound = true;
        }
        else{
            shared::hatFound = false;
        }
        if(shared::hatFound)
        {
            rectangle(original, face_i, CV_RGB(0, 255,0), 1);
            // Create the text we will annotate the box with:
            box_text = "hat ON";
            // Calculate the position for annotated text (make sure we don't
            // put illegal values in there):255
            faceDetect::faceposX = face_i.tl().x;
            faceDetect::faceposY = face_i.tl().y;
            int posx = std::max(face_i.tl().x - 10, 0);
            int posy = std::max(face_i.tl().y - 10, 0);
            // And now put it into the image:
            putText(original, box_text, Point(posx, posy), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
        }
        else{
            rectangle(original, face_i, CV_RGB(255, 0,0), 1);
            box_text = "hat OFF";
            faceDetect::faceposX = face_i.tl().x;
            faceDetect::faceposY = face_i.tl().y;
            int posx = std::max(face_i.tl().x - 10, 0);
            int posy = std::max(face_i.tl().y - 10, 0);
            // And now put it into the image:
            putText(original, box_text, Point(posx, posy), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,0,0), 2.0);
        }

    }
}

faceDetect::~faceDetect()
{

}



