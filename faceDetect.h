#ifndef FACEDETECT_H
#define FACEDETECT_H
#include "shared.h"

#include <iostream>
#include <fstream>
#include <sstream>

class faceDetect{

public:
    static int faceposX;
    static int faceposY;
    Ptr<FaceRecognizer> model;
    CascadeClassifier haar_cascade;


public:
    void initialize();
    void lbph_facedetect(IplImage* frame);
    faceDetect();
   ~faceDetect();



private:

};







#endif // FACEDETECT_H
