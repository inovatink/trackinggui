#include "tracking.h"

int tracking::lastX = -1;
int tracking::lastY = -1;


void tracking::initialize(IplImage *frame)
{

    //do the initialize here for camshift
    imgHSV = cvCloneImage(frame);
    imgHUE = cvCreateImage(cvGetSize(frame), 8, 1 );
    imgHUE->origin = frame->origin;
    masked = cvCloneImage(imgHUE);
    backprojected = cvCloneImage(imgHUE);
    cvZero(backprojected);

    selection.width = 0;
    selection.height = 0;

    hdims = 16;
    float hranges_arr[] = {0,180};
    hranges = hranges_arr;
    histM = cvCreateHist( 1, &hdims, CV_HIST_ARRAY, &hranges, 1 );
    histL = cvCreateHist( 1, &hdims, CV_HIST_ARRAY, &hranges, 1 );
    histimgM = cvCreateImage( cvSize(320,200), 8, 3 );
    histimgL = cvCreateImage( cvSize(320,200), 8, 3 );
    cvZero( histimgM );
    cvZero( histimgL );



    //colorThresholding hsv interval
    //cvInRangeS(imgHSV, cvScalar(0,211,51), cvScalar(9,256,165), imgThresh);

    //my dumb hat color variables
//    vmin = 77;
//    vmax = 179;
//    smin = 186;
    vmin = shared::minV;
    vmax = shared::maxV;
    smin = shared::minS;

    histogramCaptured = false; // have we a histogram model to track ?
    showBackProjection = false; // have we a histogram model to track ?
    firstflag = true;

}

tracking::tracking()
{
}

//utility func (make a utility.cpp later)
static CvScalar hue2rgb( float hue )
{
    int rgb[3], p, sector;
    static const int sector_data[][3]=
    {{0,2,1}, {1,2,0}, {1,0,2}, {2,0,1}, {2,1,0}, {0,1,2}};
    hue *= 0.033333333333333333333333333333333f;
    sector = cvFloor(hue);
    p = cvRound(255*(hue - sector));
    p ^= sector & 1 ? 255 : 0;

    rgb[sector_data[sector][0]] = 255;
    rgb[sector_data[sector][1]] = 0;
    rgb[sector_data[sector][2]] = p;

    return cvScalar(rgb[2], rgb[1], rgb[0],0);
}

IplImage* tracking::GetThresholdedImage(IplImage* imgHSV){
    IplImage* imgThresh=cvCreateImage(cvGetSize(imgHSV),IPL_DEPTH_8U, 1);
    // in order to track my dumb hat
    //cvInRangeS(imgHSV, cvScalar(0,211,51), cvScalar(9,256,165), imgThresh);
    cvInRangeS(imgHSV, cvScalar(shared::minH,shared::minS,shared::minV), cvScalar(shared::maxH,shared::maxS,shared::maxV), imgThresh);
    return imgThresh;
}

void tracking::ColorThresholding(IplImage* rframe){
    //thresholding
    IplImage *image_copy = cvCreateImage(cvSize(rframe->width,rframe->height), rframe->depth, rframe->nChannels);
    cvSmooth(rframe, image_copy, CV_GAUSSIAN,3,3); //smooth the original image using Gaussian kernel
    IplImage* imgHSV = cvCreateImage(cvGetSize(image_copy), IPL_DEPTH_8U, 3);
    cvCvtColor(image_copy, imgHSV, CV_BGR2HSV); //Change the color format from BGR to HSV
    IplImage* imgThresh = GetThresholdedImage(imgHSV);
    cvSmooth(imgThresh, imgThresh, CV_GAUSSIAN,3,3); //smooth the binary image using Gaussian kernel

    // Calculate the moments of 'imgThresh'
    CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
    cvMoments(imgThresh, moments, 1);
    double moment10 = cvGetSpatialMoment(moments, 1, 0);
    double moment01 = cvGetSpatialMoment(moments, 0, 1);
    double area = cvGetCentralMoment(moments, 0, 0);
    // if the area<75, I consider that the there are no object in the image and it's because of the noise, the area is not zero
    if(area>75){
        // calculate the positions of the ball
        int posX = moment10/area;
        int posY = moment01/area;
        if(lastX>=0 && lastY>=0 && posX>=0 && posY>=0)
        {
            // Draw a yellow line from the previous point to the current point
           // cvLine(shared::rImg, cvPoint(posX, posY), cvPoint(tracking::lastX, tracking::lastY), cvScalar(0,0,255), 4);
        }

        tracking::lastX = posX;
        tracking::lastY = posY;

        origin = cvPoint(tracking::lastX,tracking::lastY);
        selection = cvRect(tracking::lastX,tracking::lastY,50,50);
        selection.x = origin.x - 50 / 2;
        selection.y = origin.y - 50 / 2;
        histogramCaptured = false;

    }
    free(moments);
    cvReleaseImage(&image_copy);
    cvReleaseImage(&imgHSV);
    cvReleaseImage(&imgThresh);
}

void tracking::opencvCamShift(IplImage* incoming)
{
    //take the height and the origin of the incoming frame
    image_height = incoming->origin;
    image_origin = incoming->origin;

    //calculate imgHSV from incoming frame
    cvCvtColor( incoming, imgHSV, CV_BGR2HSV );

    //create a mask for all values within S channel value {smin ... 256}
    //and V channel value {vmin .. vmax}
    cvInRangeS( imgHSV, cvScalar(0, smin, min(vmin,vmax),0),cvScalar(180, 256, max(vmin,vmax),0), masked );

    //camshift  hsv intervals
    //minHSV
    //0
    //186
    //77
    //maxHSV
    //180
    //256
    //179

    // isolate the hue channel
    cvSplit( imgHSV, imgHUE, NULL, NULL, NULL );

    //if object is out of scope try to find it again with color thresholding
    if(firstflag)
        ColorThresholding(incoming);
    firstflag = false;

    //((track_box.size.height * track_box.size.width) != 0) &&
    if(((track_box.size.height * track_box.size.width) < 75) )
    {
        ColorThresholding(incoming);
        //printf("area : %f\n",track_box.size.height * track_box.size.width);
    }

    if((selection.width > 0) && (selection.height > 0))
    {
        // first checking it is valid (>0) and within the image bounds
        selection.width = min( selection.width, incoming->width );
        selection.height = min( selection.height, incoming->height );

        // set it as a Region of Interest (ROI) in the image
        cvSetImageROI( incoming, selection );

        // use XOR with a scalar (255 for all channels to mark it out)
        cvXorS( incoming, cvScalarAll(255), incoming, 0 );

        // reset ROI for image
        cvResetImageROI( incoming );
    }

    // if we have selected a new template area within the image
    //(!(select_object)) &&
    if ( (selection.width > 0) && (selection.height > 0) &&
         (!histogramCaptured))
    {

        // get the histogram for the Hue channel
        // within the areas defined Saturation and Variance Channel mask

        float max_val = 0.f;
        cvSetImageROI( imgHUE, selection );
        cvSetImageROI( masked, selection );
        cvCalcHist( &imgHUE, histM, 0, masked );
        cvGetMinMaxHistValue( histM, 0, &max_val, 0, 0 );
        cvConvertScale( histM->bins, histM->bins, max_val ? 255. / max_val : 0., 0 );
        cvResetImageROI( imgHUE );
        cvResetImageROI( masked );

        // set the initial tracking position of the object
        track_window = selection;
        // create the histogram image of model
        cvZero( histimgM );
        histogramCaptured = true;
    }

    // if we have a selected template perform cross correlation template matching

    if (histogramCaptured){

        // calculate the back-projection of the histogram on the image
        cvCalcBackProject( &imgHUE, backprojected, histM );
        cvAnd( backprojected, masked, backprojected, 0 );

        // pass this backprojection to the camShift Routine
        cvCamShift(backprojected, track_window,
                   cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ),
                   &track_comp, &track_box );

        // update the tracking window for the next iteration based
        // on the output of the current predicted position

        track_window = track_comp.rect;
        tracking::lastX = track_window.x;
        tracking::lastY = track_window.y;
        shared::hatposX = tracking::lastX;
        shared::hatposY = tracking::lastY;

        // draw the detected result (taking into account image origin)
        if( rframe->origin ) {
            track_box.angle = -track_box.angle;
        }

        cvEllipseBox( incoming, track_box, CV_RGB(0,0,255), 3, CV_AA, 0 );

        // draw the histogram of the detected result area
        float max_val = 0.f;
        cvSetImageROI( imgHUE, track_window );
        cvSetImageROI( masked, track_window );
        cvCalcHist( &imgHUE, histL, 0, masked );
        cvGetMinMaxHistValue( histL, 0, &max_val, 0, 0 );
        cvConvertScale( histL->bins, histL->bins, max_val ? 255. / max_val : 0., 0 );
        cvResetImageROI( imgHUE );
        cvResetImageROI( masked );

        // create the histogram image of model
        cvZero( histimgL );
    }


}

tracking::~tracking()
{
    cvReleaseImage ( &rframe);
    cvReleaseImage ( &backprojected );
    cvReleaseImage ( &masked  );
    cvReleaseImage ( &imgHSV );
    cvReleaseImage ( &imgHUE);
    cvReleaseImage ( &histimgM);
    cvReleaseImage ( &histimgL);
}
