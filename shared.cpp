#include "shared.h"

IplImage  *shared::frame     = cvCreateImage( cvSize(640,480), IPL_DEPTH_8U, 3);
IplImage  *shared::binaryImg = cvCreateImage( cvSize(640,480), IPL_DEPTH_8U, 1);
IplImage  *shared::colorImg  = cvCreateImage( cvSize(640,480), IPL_DEPTH_8U, 3);
Mat shared::matframe;
Rect shared::initialCond;

//slider color interval values (colorThresholding parameters)
//int shared::minH = 0;
//int shared::minS = 211;
//int shared::minV = 51;
//int shared::maxH = 9;
//int shared::maxS = 256;
//int shared::maxV = 165;

//yellow baret parameters
int shared::minH = 21;
int shared::minS = 57;
int shared::minV = 114;
int shared::maxH = 65;
int shared::maxS = 235;
int shared::maxV = 256;


int shared::hatposX = 0;
int shared::hatposY = 0;

bool shared::startTracking = false;
bool shared::hatFound = false;


