#ifndef DETECTION_H
#define DETECTION_H

#include "shared.h"
#include <iostream>
#include <fstream>
#include <sstream>

class detection{

public:
    static int faceposX;
    static int faceposY;
    Ptr<FaceRecognizer> model;
    CascadeClassifier haar_cascade;


public:
    void initialize_FaceDetect();
    void lbph_FaceDetect(IplImage* frame);

    void initialize_HumanDetect_HOG();
    void HOG_HumanDetect(IplImage* frame);

    detection();
   ~detection();



private:
    HOGDescriptor hog;
    Mat hogImg;



};




#endif // DETECTION_H
