#ifndef SHARED_H
#define SHARED_H

#define colorFiltering 0

#include <opencv2/core/core.hpp>
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"


#include <opencv/cv.h>
#include <algorithm>
#include <stdio.h>

using namespace std;
using namespace cv;

class shared{
public:

static IplImage *frame;
static IplImage *binaryImg;
static IplImage *colorImg;
static Rect initialCond;
static Mat matframe;
static bool startTracking;
static bool hatFound;

static int hatposX;
static int hatposY;

static int minH;
static int minS;
static int minV;
static int maxH;
static int maxS;
static int maxV;
};

#endif // SHARED_H
