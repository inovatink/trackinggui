#ifndef TRACKING_H
#define TRACKING_H

#include "shared.h"

IplImage* GetThresholdedImage(IplImage* imgHSV);

class tracking
{
private:
    //color thresholding param
    IplImage* rframe;
    IplImage* imgHSV;

    IplImage* imgHUE;
    IplImage* masked;
    IplImage* backprojected;

    CvRect track_window;			// tracking window used by CAMSHIFT
    CvBox2D track_box;			// tracking box used by CAMSHIFT
    CvConnectedComp track_comp;   // tracking component used by CAMSHIFT


    bool histogramCaptured; // have we a histogram model to track
    bool showBackProjection; // have we a histogram model to track


    //allocating all required histogram buffers
    int hdims;
    float* hranges;
    CvHistogram* histM;
    CvHistogram* histL;
    IplImage* histimgM;
    IplImage* histimgL;

    int image_height;		// height of image
    int image_origin;		// image origin
    CvRect selection;		// the area selected
    int select_object;	// has an area been selected
    CvPoint origin;			// the origin of the selection
    bool firstflag;

    //preprocessed results for the orange cap
    int vmin;
    int vmax;
    int smin;


public:
    static int lastX, lastY;


public:
    tracking();
    void initialize(IplImage* rframe);
    void ColorThresholding(IplImage* rframe);
    void opencvCamShift(IplImage* rframe);
    IplImage* GetThresholdedImage(IplImage* imgHSV);
    void initialize();

    ~tracking();
};

#endif // TRACKING_H
