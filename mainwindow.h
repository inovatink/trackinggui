#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>

#include "shared.h"
#include "tracking.h"
#include "detection.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
    CvCapture* capture;
    QTimer* refreshTimer;
    tracking* tra;
    detection* detect;
    bool initThis;

private slots:
    void UpdateGUI();
//    void on_pushButton_1_clicked();
//    void on_slider_h_valueChanged(int value);
//    void on_slider_s_valueChanged(int value);
//    void on_slider_v_valueChanged(int value);
//    void on_slider_h_2_valueChanged(int value);
//    void on_slider_s_2_valueChanged(int value);
//    void on_slider_v_2_valueChanged(int value);
};

#endif // MAINWINDOW_H
