#ifndef __SIMPLE_CAMSHIFT_WRAPPER_H
#define __SIMPLE_CAMSHIFT_WRAPPER_H

#include <cv.h>
#include <stdio.h>
#include "shared.h"

class OpenCVMeanShift
{
private:
	// Parameters
	static int   nHistBins;                 // number of histogram bins
	static float rangesArr[];          // histogram range
	static int vmin, vmax, smin; // limits for calculating hue
	float * pRanges;
	bool isInitialized;
	static int maxLoopCount;

	// File-level variables
	IplImage * pHSVImg  ; // the input image converted to HSV color mode
	IplImage * pHueImg  ; // the Hue channel of the HSV image
	IplImage * pMask    ; // this image is used for masking pixels
	IplImage * pProbImg ; // the face probability estimates for each pixel
	CvHistogram * pHist ; // histogram of hue in the original face image
	CvRect prevFaceRect;  // location of face in previous frame
	CvBox2D faceBox;      // current face-location estimate
	int nFrames;

public:
	OpenCVMeanShift(const IplImage * pImg);
	void releaseTracker();
	void startTracking(IplImage * pImg, CvRect * pFaceRect);
    CvBox2D CamShifttrack(IplImage * pImg);
	CvRect MeanShiftTrack(IplImage * pImg);
	void setVmin(int _vmin);
	void setSmin(int _vmin);
	void setLoopCount(int count);
private:
	void updateHueImage(const IplImage * pImg);
};

#endif
