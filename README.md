# trackingGUI #
trackingGUI is a QT Project which uses OpenCV's Camshift algorithm in order to track a specific object.

In order to detect the object, simple colorFiltering technique is used (the rectangle). After detecting the target object, Camshift algorithm takes the selected area as an input and starts to track the object. When the object dissappears from the FOV, colorFiltering is used again to give an other initialization input to the camshift function.