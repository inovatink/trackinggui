#include "OpenCVMeanShift.h"

int   OpenCVMeanShift::nHistBins = 30;                 // number of histogram bins
float OpenCVMeanShift::rangesArr[] = {0,180};          // histogram range
int OpenCVMeanShift::vmin = 65;
int OpenCVMeanShift::vmax = 256;
int OpenCVMeanShift::smin = 55; // limits for calculating hue
int OpenCVMeanShift::maxLoopCount = 10;

OpenCVMeanShift::OpenCVMeanShift(const IplImage * pImg)
{
	isInitialized = false;
	nFrames = 0;
	// Allocate the main data structures ahead of time
	float * pRanges = rangesArr;
	pHSVImg  = cvCreateImage( cvGetSize(pImg), 8, 3 );
	pHueImg  = cvCreateImage( cvGetSize(pImg), 8, 1 );
	pMask    = cvCreateImage( cvGetSize(pImg), 8, 1 );
	pProbImg = cvCreateImage( cvGetSize(pImg), 8, 1 );
	pHist = cvCreateHist( 1, &nHistBins, CV_HIST_ARRAY, &pRanges, 1 );
}

void OpenCVMeanShift::releaseTracker()
{
	// Release all tracker resources
	cvReleaseImage( &pHSVImg );
	cvReleaseImage( &pHueImg );
	cvReleaseImage( &pMask );
	cvReleaseImage( &pProbImg );

	cvReleaseHist( &pHist );
}

void OpenCVMeanShift::startTracking(IplImage * pImg, CvRect * pFaceRect)
{
	if ( !isInitialized){
		float maxVal = 0.f;

		// Create a new hue image
		updateHueImage(pImg);

		// Create a histogram representation for the face
		cvSetImageROI( pHueImg, *pFaceRect );
		cvSetImageROI( pMask,   *pFaceRect );
		cvCalcHist( &pHueImg, pHist, 0, pMask );
		cvGetMinMaxHistValue( pHist, 0, &maxVal, 0, 0 );
		cvConvertScale( pHist->bins, pHist->bins, maxVal? 255.0/maxVal : 0, 0 );
		cvResetImageROI( pHueImg );
		cvResetImageROI( pMask );

		// Store the previous face location
		prevFaceRect = *pFaceRect;
		isInitialized = true;
	}
}

CvBox2D OpenCVMeanShift::CamShifttrack(IplImage * pImg)
{
	CvConnectedComp components;

	// Create a new hue image
	updateHueImage(pImg);

	// Create a probability image based on the face histogram
	cvCalcBackProject( &pHueImg, pProbImg, pHist );
	cvAnd( pProbImg, pMask, pProbImg, 0 );

	// Use CamShift to find the center of the new face probability
	cvCamShift( pProbImg, prevFaceRect,cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, maxLoopCount, 1 ),&components, &faceBox );

	if (components.rect.width > 10000 || components.rect.height > 10000 ){
		throw 6;
	}
	// Update location and angle
	prevFaceRect = components.rect;
	faceBox.angle = -faceBox.angle;
	return faceBox;
}

CvRect OpenCVMeanShift::MeanShiftTrack(IplImage * pImg)
{
	CvConnectedComp components;

	// Create a new hue image
	updateHueImage(pImg);

	// Create a probability image based on the face histogram
	cvCalcBackProject( &pHueImg, pProbImg, pHist );
	cvAnd( pProbImg, pMask, pProbImg, 0 );

	// Use MeanShift to find the center of the new face probability
	cvMeanShift(pProbImg, prevFaceRect, cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, maxLoopCount, 1 ), &components);
	// Update face location and angle
	prevFaceRect = components.rect;

	return prevFaceRect;
}

void OpenCVMeanShift::updateHueImage(const IplImage * pImg)
{
	// Convert to HSV color model
	cvCvtColor( pImg, pHSVImg, CV_BGR2HSV );

	// Mask out-of-range values
	//cvInRangeS( pHSVImg, cvScalar(0, smin, MIN(vmin,vmax), 0),	cvScalar(180, 256, MAX(vmin,vmax) ,0), pMask );

	// Extract the hue channel
	cvSplit( pHSVImg, pHueImg, 0, 0, 0 );
}

void OpenCVMeanShift::setVmin(int _vmin)
{ vmin = _vmin; }


void OpenCVMeanShift::setSmin(int _smin)
{ smin = _smin; }

void OpenCVMeanShift::setLoopCount(int count)
{
	if ( count > 0){
		this->maxLoopCount = count;
	}
}
