#-------------------------------------------------
#
# Project created by QtCreator 2013-10-07T17:08:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = trackGUI
TEMPLATE = app

LIBS += `pkg-config opencv --libs`


CONFIG += link_pkgconfig
PKGCONFIG += opencv

SOURCES += main.cpp\
        mainwindow.cpp \
    tracking.cpp \
    shared.cpp \
    detection.cpp

HEADERS  += mainwindow.h \
    tracking.h \
    shared.h \
    detection.h

FORMS    += mainwindow.ui
