#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore>

QImage IplImage2QImage(const IplImage *iplImage)
{
    int height = iplImage->height;
    int width = iplImage->width;

    if  (iplImage->depth == IPL_DEPTH_8U && iplImage->nChannels == 3)
    {
        const uchar *qImageBuffer = (const uchar*)iplImage->imageData;
        QImage img(qImageBuffer, width, height, QImage::Format_RGB888);
        return img.rgbSwapped();
    } else if  (iplImage->depth == IPL_DEPTH_8U && iplImage->nChannels == 1){
        const uchar *qImageBuffer = (const uchar*)iplImage->imageData;
        QImage img(qImageBuffer, width, height, QImage::Format_Indexed8);

        QVector<QRgb> colorTable;
        for (int i = 0; i < 256; i++){
            colorTable.push_back(qRgb(i, i, i));
        }
        img.setColorTable(colorTable);
        return img;
    }else{
        qWarning() << "Image cannot be converted.";
        return QImage();
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initThis = false;

                capture = cvCaptureFromAVI("/home/inovatink/Desktop/dataset_insaat/last_cropped/07_cropped_data.avi");
                if (!cvGrabFrame(capture)) {
                    printf("Capture failure\n");
                    exit(0);
                }

    //in order to capture from camw
//    capture = cvCaptureFromCAM(0);
//    if(!capture){
//        printf("Capture failure\n");
//        exit(0);
//    }

    //get a frame from query for every timer cycle
    shared::frame  = cvQueryFrame(capture);
    if(!shared::frame)  return;

    tra = new tracking();
    detect = new detection();
    //detect->initialize_FaceDetect();
    detect->initialize_HumanDetect_HOG();

    refreshTimer = new QTimer(this);
    connect(refreshTimer,SIGNAL(timeout()),this,SLOT(UpdateGUI()));
    refreshTimer->start(5);


}

//This Function updates the UI wrt to the refreshTimer
//All the algorithms are called from here
void MainWindow::UpdateGUI()
{

    //get a frame from query for every timer cycle
    shared::frame  = cvQueryFrame(capture);
    if(!shared::frame)  return;

    //these are generated dynamically in order to be used on converting the frame query to a reasonable size
    IplImage *rframe = cvCreateImage(cvSize(320,240),shared::frame->depth, shared::frame->nChannels );
    IplImage *rframe2 = cvCreateImage(cvSize(320,240),shared::frame->depth, shared::frame->nChannels );

    //resize the frame
    cvResize(shared::frame, rframe);
    cvResize(shared::frame, rframe2);

    //the boolean type are used initThis and startTracking
    //at start both are 0, means the tracking is not initialized and tracking is not started

    // (initThis,startTracking) = (0,0)
    if( !initThis && !shared::startTracking)
    {
        tra->initialize(rframe);
        initThis = 1;
        shared::startTracking = 1;
    }
    //algorithms will be listed here
    // (initThis,startTracking) = (1,1)
    if(shared::startTracking)
        //tra->opencvCamShift(rframe);

    //detect->lbph_FaceDetect(rframe2);
    detect->HOG_HumanDetect(rframe2);

    QImage tracked = IplImage2QImage(rframe);
    QImage detected = IplImage2QImage(rframe2);

    //updating ui accordingly
    ui->origLabel->setPixmap(QPixmap::fromImage(tracked));
    ui->filterLabel->setPixmap(QPixmap::fromImage(detected));

//    QString xposHat = QString("Hat X Coor: %1").arg(tracking::lastX);
//    QString yposHat = QString("Hat Y Coor: %1").arg(tracking::lastY);
//    QString xposFace = QString("Face X Coor: %1").arg(detection::faceposX);
//    QString yposFace = QString("Face Y Coor: %1").arg(detection::faceposY);

    //clear the text areas, push the updated results
//    ui->hat_position->clear();
//    ui->hat_position->appendPlainText(xposHat);
//    ui->hat_position->appendPlainText(yposHat);

//    ui->face_position->clear();
//    ui->face_position->appendPlainText(xposFace);
//    ui->face_position->appendPlainText(yposFace);

    cvReleaseImage(&rframe);
    cvReleaseImage(&rframe2);
    cvReleaseImage(&shared::binaryImg);
   }

MainWindow::~MainWindow()
{
    delete ui;
    cvReleaseImage(&shared::frame);

}

//push button to stop/start tracking
//void MainWindow::on_pushButton_1_clicked()
//{
//    if(!shared::startTracking)
//    {
//        ui->pushButton_1->setText("tracking is running");
//        initThis = 0;
//        // (initThis,startTracking) = (0,0)
//    }
//    else{
//        ui->pushButton_1->setText("tracking stopped");
//        shared::startTracking = 0;
//        // (initThis,startTracking) = (1,0)
//    }
//}

//HSV Sliders, dynamically updates the limits of HSV interval
//void MainWindow::on_slider_h_valueChanged(int value)
//{
//    shared::minH = value;
//}

//void MainWindow::on_slider_s_valueChanged(int value)
//{
//    shared::minS = value;
//}

//void MainWindow::on_slider_v_valueChanged(int value)
//{
//    shared::minV = value;
//}

//void MainWindow::on_slider_h_2_valueChanged(int value)
//{
//    shared::maxH = value;
//}

//void MainWindow::on_slider_s_2_valueChanged(int value)
//{
//    shared::maxS = value;
//}

//void MainWindow::on_slider_v_2_valueChanged(int value)
//{
//    shared::maxV = value;
//}
